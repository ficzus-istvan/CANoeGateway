var CronJob = require('cron').CronJob;
var winston = require('./logger.js');
var db = require('./db');
var moment = require('moment');

var logger = winston.loggers.get('console');

module.exports.dummy = new CronJob('0 0 18 * * *', function() { // every day at 6PM
  logger.silly('Executing job [dummy]');
}, null, true);

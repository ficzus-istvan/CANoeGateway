var winston = require('winston');
require('winston-daily-rotate-file');

var transportError = new winston.transports.DailyRotateFile({
  filename: 'logs/errors.log',
  datePattern: 'yyyy-MM-dd.',
  prepend: true,
  level: 'error'
});

var transportDebug = new winston.transports.DailyRotateFile({
  filename: 'logs/debug.log',
  datePattern: 'yyyy-MM-dd.',
  prepend: true,
  level: 'silly'
});

var transporConsole = new winston.transports.Console({
  timestamp: true,
  colorize: true,
  level: 'silly'
});

winston.loggers.add('console', {
  transports: [
    transportError,
    transporConsole
  ]
});

winston.loggers.add('debug', {
  transports: [
    transportDebug
  ]
});

module.exports = winston;

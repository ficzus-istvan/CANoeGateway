var express = require('express');
var passport = require('passport');
var router = express.Router();
var nconf = require('nconf');
nconf.file({ file: 'config.json', search: true });
var winston = require('../logger.js');
var db = require('../db');
var roles = require('../roles.js');
const { exec } = require('child_process');

router.get('/', function (req, res, next) {
	var devices = [];

	exec('usbsrv -l', (err, stdout, stderr) => {
		if (err) {
			// node couldn't execute the command
			return res.render('error', { message: err });
		}

		console.log(stdout);
		var lines = stdout.toString().split('\n');
		for (var i = 0; i < lines.length; i++) {
			var l = lines[i].trim();
			if (l.match(/^\d+: Vector/)) {
				var device = lines[i].trim() + '\n' + lines[i + 1].trim() + '\n' + lines[i + 2].trim();
				devices.push(device);
			}
		}

		res.render('index', { data: devices });
	});
  /*if (req.isAuthenticated()) {
    if (req.user.role === 'arobs') {
      res.redirect('/arobs');
    } else {
      res.render('index');
    }
  } else {
    res.redirect('/login');
  }*/
});

router.post('/api/share', function (req, res, next) {
	var id = req.body.id;
	exec('usbsrv -share ' + id, (err, stdout, stderr) => {
		if (err) {
			// node couldn't execute the command
			return res.json({ res: false, id: id, err: err });
		}
		if (stdout.toString().match(/OPERATION SUCCESSFUL/)) {
			return res.json({ res: true, id: id });
		} else {
			return res.json({ res: false, id: id, stdout: stdout });
		}
	});
});

router.post('/api/unshare', function (req, res, next) {
	var id = req.body.id;
	exec('usbsrv -unshare ' + id, (err, stdout, stderr) => {
		if (err) {
			// node couldn't execute the command
			return res.json({ res: false, id: id, err: err });
		}
		if (stdout.toString().match(/OPERATION SUCCESSFUL/)) {
			return res.json({ res: true, id: id });
		} else {
			return res.json({ res: false, id: id, stdout: stdout });
		}
	});
});

router.get('/register', function (req, res) {
	res.render('authentication/register', { roles: roles, message: req.flash('signupMessage') });
});

router.post('/register', passport.authenticate('local-signup', {
	successRedirect: '/', // redirect to the secure profile section
	failureRedirect: '/register', // redirect back to the signup page if there is an error
	failureFlash: true // allow flash messages
}));

router.get('/login', function (req, res) {
	res.render('authentication/login', { message: req.flash('loginMessage') });
});

router.post('/login', passport.authenticate('local-login', {
	successRedirect: '/', // redirect to the secure profile section
	failureRedirect: '/login', // redirect back to the signup page if there is an error
	failureFlash: true // allow flash messages
}),
	function (req, res) {
		if (req.body.remember) {
			req.session.cookie.maxAge = 1000 * 60 * 3;
		} else {
			req.session.cookie.expires = false;
		}
		res.redirect('/');
	});

router.get('/logout', function (req, res) {
	req.logout();
	res.redirect('/');
});

module.exports = router;

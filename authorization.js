var ConnectRoles = require('connect-roles');

var roles = new ConnectRoles({
    failureHandler: function (req, res, action) {
    // optional function to customise code that runs when
    // user fails authorisation
    var accept = req.headers.accept || '';
    res.status(403);
    if (~accept.indexOf('html')) {
      res.render('error/access-denied', {action: action});
    } else {
      res.send('Access Denied - You don\'t have permission to: ' + action);
    }
  }
});

//anonymous users can only access the anonymous pages
//returning false stops any more rules from being
//considered
roles.use(function (req, action) {
  if (!req.isAuthenticated()) return action === 'access anonymous page';
});

roles.use('access arobs page', function (req) {
  if (req.user.role === 'arobs') {
    return true;
  }
});

//admin users can access all pages
roles.use(function (req) {
  if (req.user.role === 'admin') {
    return true;
  }
});

module.exports = roles;

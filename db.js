var mysql = require('mysql');
var winston = require('./logger.js');
var moment  =require('moment');
var nconf = require('nconf');
nconf.file({ file: 'config.json', search: true });

var logger = winston.loggers.get('console');

var pool = null;

exports.connect = function(done) {
  pool = mysql.createPool({
    host: nconf.get('db_host'),
    user: nconf.get('db_user'),
    password: nconf.get('db_password'),
    database: nconf.get('db_name')
  });
  done();
};

exports.get = function() {
  return pool;
};

$(function () {
	$("a.list-group-item").each(function (index) {
		var txt = $(this).text();
		if (txt.match(/not plugged, shared/)) {
			$(this).addClass('list-group-item-success');
		} else
		if (txt.match(/plugged, shared/)) {
			$(this).addClass('list-group-item-info');
		} else
		if (txt.match(/not plugged, unshared/)) {
			$(this).addClass('list-group-item-warning');
		} else
		if (txt.match(/in use by/)) {
			$(this).addClass('list-group-item-danger');
		}
	});

	$('#myModal').on('hidden.bs.modal', function (e) {
		location.reload();
	});

	$('a.list-group-item').click(function () {
		var _this = $(this);
		var txt = $(this).text();
		var deviceId = txt.match(/\d+/)[0];

		if (txt.match(/plugged, shared/) || txt.match(/in use by/)) {
			var unshare = $.post('/api/unshare', { id: deviceId });

			unshare.done(function (data) {
				console.log(data);
				if (data.res == true) {
					$("#myModal .modal-title").text('Successfull');
					$("#myModal .msg").text('Device with ID [' + deviceId + '] unshared');
					$("#myModal").modal();
				} else {
					$("#myModal .modal-title").text('Failed');
					$("#myModal .msg").text(data.err);
					$("#myModal").modal();
				}
			});
		} else {
			var share = $.post('/api/share', { id: deviceId });

			share.done(function (data) {
				console.log(data);
				if (data.res == true) {
					$("#myModal .modal-title").text('Successfull');
					$("#myModal .msg").text('Device with ID[' + deviceId + '] shared');
					$("#myModal").modal();
				} else {
					$("#myModal .modal-title").text('Failed');
					$("#myModal .msg").text(data.err);
					$("#myModal").modal();
				}
			});
		}
	});
});

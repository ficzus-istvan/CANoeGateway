var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var bcrypt = require('bcrypt');
var flash = require('connect-flash');
var roles = require('./authorization.js');
var jobs = require('./jobs');

var nconf = require('nconf');
nconf.file({ file: 'config.json', search: true });

var index = require('./routes/index');

var db = require('./db');

// Connect to MySQL on start
db.connect(function(err) {
  if (err) {
    winston.error('Unable to connect to MySQL.');
    process.exit(1);
  }
});

passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  db.get().query("SELECT * FROM users WHERE id = ? ",[id], function(err, rows){
    done(err, rows[0]);
  });
});

passport.use(
  'local-signup',
  new LocalStrategy({
    // by default, local strategy uses username and password, we will override with email
    usernameField : 'username',
    passwordField : 'password',
    passReqToCallback : true // allows us to pass back the entire request to the callback
  },
  function(req, username, password, done) {
    // find a user whose email is the same as the forms email
    // we are checking to see if the user trying to login already exists
    db.get().query("SELECT * FROM users WHERE username = ?",[username], function(err, rows) {
      if (err)
        return done(err);
      if (rows.length) {
        return done(null, false, req.flash('signupMessage', 'That username is already taken.'));
      } else {
        // if there is no user with that username
        // create the user
        var newUserMysql = {
          username: username,
          password: bcrypt.hashSync(password, 2), // use the generateHash function in our user model
          role: req.body.role
        };

        var insertQuery = "INSERT INTO users ( username, password, role ) values (?,?,?)";

        db.get().query(insertQuery,[newUserMysql.username, newUserMysql.password, newUserMysql.role],function(err, rows) {
          newUserMysql.id = rows.insertId;
          return done(null, newUserMysql);
        });
      }
    });
  })
);

passport.use(
  'local-login',
  new LocalStrategy({
    // by default, local strategy uses username and password, we will override with email
    usernameField : 'username',
    passwordField : 'password',
    passReqToCallback : true // allows us to pass back the entire request to the callback
  },
  function(req, username, password, done) { // callback with email and password from our form
    db.get().query("SELECT * FROM users WHERE username = ?",[username], function(err, rows){
      if (err)
        return done(err);
      if (!rows.length) {
        return done(null, false, req.flash('loginMessage', 'No user found.')); // req.flash is the way to set flashdata using connect-flash
      }

      // if the user is found but the password is wrong
      if (!bcrypt.compareSync(password, rows[0].password))
        return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.')); // create the loginMessage and save it to session as flashdata

      // all is well, return successful user
      return done(null, rows[0]);
    });
  })
);

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
//app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(require('express-session')({
        secret : 'myessolutions',
        resave : false,
        saveUninitialized : false
    }));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash()); // use connect-flash for flash messages stored in session
app.use(express.static(path.join(__dirname, 'public')));
app.use('/bower_components',  express.static(__dirname + '/bower_components'));

app.use(roles.middleware());

app.use('/', index);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

app.set('myport', process.env.PORT || 5000);

var server = app.listen(app.get('myport'), function() {
  console.log('Express server listening on port ' + server.address().port);
});
